CREATE OR REPLACE TABLE settings (
    id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    name VARCHAR(100) NOT NULL,
    scope VARCHAR(10) NOT NULL DEFAULT 'global',
    scopeId INTEGER NOT NULL DEFAULT 0,
    value VARCHAR(10000) ALLOCATE(100) NOT NULL DEFAULT '',
    changed TIMESTAMP FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP NOT NULL,
    changedBy VARCHAR(100) ALLOCATE(10),
    version INTEGER NOT NULL DEFAULT 1,
    PRIMARY KEY (id),
    UNIQUE(name, scope, scopeId)
) RCDFMT settingsr;

LABEL ON TABLE settings IS 'Settings';
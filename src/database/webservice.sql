CREATE OR REPLACE TABLE webService (
    id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    name VARCHAR(50) NOT NULL,
    description FOR COLUMN desc VARCHAR(1000) NOT NULL DEFAULT '',
    group INT NOT NULL
      REFERENCES webServiceGroup(id) ON DELETE CASCADE,
    icon varchar(50) NOT NULL DEFAULT 'cog',
    program VARCHAR(50) NOT NULL,
    submitJobTemplate VARCHAR(250) NOT NULL,
    configurable INT NOT NULL DEFAULT 0,
    autostart INT NOT NULL DEFAULT 0,
    maxInstances INT NOT NULL DEFAULT 1,
    version INT NOT NULL DEFAULT 0,
    changed TIMESTAMP FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP NOT NULL,
    changedBy VARCHAR(100) ALLOCATE(10),
    PRIMARY KEY (id),
    UNIQUE(group, name)
) RCDFMT websrvr1;

LABEL ON TABLE webService IS 'Web Service';
 
LABEL ON COLUMN webService ( 
    id IS 'Id', 
    name IS 'Name',
    description is 'Description',
    group IS 'Web Service Group Id',
    icon IS 'Icon',
    program IS 'Program',
    submitJobTemplate IS 'SBMJOB Template',
    configurable IS 'Uses configuration',
    autostart IS 'Number Services initially started',
    version IS 'Version'
);
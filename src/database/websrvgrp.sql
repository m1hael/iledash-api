CREATE OR REPLACE TABLE webServiceGroup FOR SYSTEM NAME websrvgrp (
    id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    name VARCHAR(50) NOT NULL,
    description FOR COLUMN desc VARCHAR(100) NOT NULL DEFAULT '',
    icon VARCHAR(50) NOT NULL DEFAULT 'settings',
    changed TIMESTAMP FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP NOT NULL,
    changedBy VARCHAR(100) ALLOCATE(10),
    PRIMARY KEY (id),
    UNIQUE(name)
) RCDFMT websrvgrpr;

LABEL ON TABLE webServiceGroup IS 'Web Service Group';
 
LABEL ON COLUMN webServiceGroup ( 
    id IS 'Id', 
    name IS 'Name',
    description IS 'Description',
    icon IS 'Icon'
); 
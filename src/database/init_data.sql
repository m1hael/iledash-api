INSERT INTO settings(name, scope, value) VALUES
	('ILEDASH_CONFIG_URL', 'global', 'http://your_ibm_i:45001/api/configuration'),
	('ILEDASH_END_JOB', 'global', 'ENDJOB JOB(${job}) OPTION(*IMMED)'),
	('ILEDASH_UPDATE_INTERVAL', 'global', '10');
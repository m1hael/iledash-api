CREATE OR REPLACE TABLE webServiceConfig FOR SYSTEM NAME websrvcfg (
    id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    service INT NOT NULL DEFAULT 0
        REFERENCES webService(id) ON DELETE CASCADE,
    urlToken VARCHAR(100) NOT NULL DEFAULT '',
    job CHAR(28) NOT NULL DEFAULT '',
    jobStatus VARCHAR(10) NOT NULL DEFAULT '',
    instance BIGINT NOT NULL DEFAULT 0,
    data CLOB(1M) ALLOCATE(1000) NOT NULL DEFAULT '{}',
    changed TIMESTAMP FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP NOT NULL,
	changedBy VARCHAR(100) ALLOCATE(10),
    PRIMARY KEY (id)
) RCDFMT websrvcfgr;

LABEL ON TABLE webServiceConfig IS 'Web Service Configuration';
 
LABEL ON COLUMN webServiceConfig (
    id IS 'Id', 
    service IS 'Web Service Id',
    urlToken IS 'URL Token',
    job IS 'Qualified Job Id',
    instance IS 'Instance Id',
    data IS 'Configuration Data'
); 
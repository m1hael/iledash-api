**FREE


/include 'iledash.rpginc'
/include 'llist/llist_h.rpgle'
/include 'message/message_h.rpgle'
/include 'osapi.rpginc'
/include 'psds.rpginc'
/include 'sqlStates.rpginc'


dcl-ds iledash_instance_data qualified template;
  serviceId int(10);
  configId int(10);
  job varchar(28);
  jobStatus varchar(10);
end-ds;
  

main();
*inlr = *on;


dcl-proc main;
  dcl-s interval int(10);
  dcl-s keepRunning ind inz(*on);
  
  interval = getInterval();
  updateCurrentJobSetting();

  dow (keepRunning);
    updateServiceInstances();
  
    doze(interval);
  enddo;
  
  on-exit;
    cleanupCurrentJobSetting();
end-proc;


dcl-proc updateServiceInstances;
  dcl-s jobs pointer;
  dcl-ds instanceData likeds(iledash_instance_data);
  dcl-s sqlError varchar(1000);
  dcl-ds data qualified;
    configId int(10);
    job varchar(28);
  end-ds;
  
  jobs = listActiveServiceInstances();

  exec sql DECLARE c2 CURSOR FOR 
               SELECT id, job 
               FROM webserviceconfig
               WHERE job <> ''
               FOR UPDATE OF job, jobStatus
               WITH NC;
  exec sql OPEN c2;
  if (sqlState <> SQL_OK);
    exec sql GET DIAGNOSTICS CONDITION 1 :sqlError = MESSAGE_TEXT;
    message_escape('Could not prepare listing config instances for update. ' + 
        sqlState + ' - ' + sqlError);
  endif;
  
  exec sql FETCH FROM c2 INTO :data;
  if (sqlState <> SQL_OK and sqlState <> SQL_NOT_FOUND);
    exec sql GET DIAGNOSTICS CONDITION 1 :sqlError = MESSAGE_TEXT;
    message_escape('Could not list config instances for update. ' + sqlState + ' - ' + sqlError);
  endif;
  
  dow (sqlState = SQL_OK);
    instanceData = getInstanceDataFor(jobs : data.job);
    if (instanceData.serviceId = 0);
      exec sql UPDATE webserviceconfig
               SET job = '' , jobStatus = ''
               WHERE CURRENT OF c2;
    else;
      exec sql UPDATE webserviceconfig
               SET jobStatus = :instanceData.jobStatus
               WHERE CURRENT OF c2;
    endif;
    if (sqlState <> SQL_OK and sqlState <> SQL_NOT_FOUND);
      exec sql GET DIAGNOSTICS CONDITION 1 :sqlError = MESSAGE_TEXT;
      message_escape('Could not clean up job in config instances. ' + 
          sqlState + ' - ' + sqlError);
    endif;
    
    exec sql FETCH FROM c2 INTO :data;
  enddo;
  
  on-exit;
    list_dispose(jobs);
    exec sql CLOSE c2;
end-proc;


dcl-proc getInstanceDataFor;
  dcl-pi *n likeds(iledash_instance_data);
    jobs pointer const;
    job varchar(28) const;
  end-pi;

  dcl-ds instanceData likeds(iledash_instance_data) based(ptr);
  dcl-ds match likeds(iledash_instance_data) inz;
  
  ptr = list_iterate(jobs);
  dow (ptr <> *null);
    if (instanceData.job = job);
      match = instanceData;
      list_abortIteration(jobs);
      leave;
    endif;
    
    ptr = list_iterate(jobs);
  enddo;  	
  
  return match;
end-proc;


dcl-proc listActiveServiceInstances;
  dcl-pi *n pointer end-pi;

  dcl-s jobs pointer;
  dcl-s sqlError varchar(1000);
  dcl-ds instanceData likeds(iledash_instance_data);
  
  jobs = list_create();
  
  exec sql DECLARE c1 CURSOR FOR 
      SELECT service AS serviceid, id AS configId, TRIM(job), job_status AS jobstatus 
      FROM TABLE(QSYS2.JOB_INFO(JOB_USER_FILTER => '*ALL')) AS ji 
          JOIN iledash.webserviceconfig AS c ON (ji.job_name = c.job)
      WHERE JOB_STATUS <> 'OUTQ';
  exec sql OPEN c1;
  if (sqlState <> SQL_OK);
    exec sql GET DIAGNOSTICS CONDITION 1 :sqlError = MESSAGE_TEXT;
    message_escape('Could not prepare getting active service instances. ' + 
        sqlState + ' - ' + sqlError);
  endif;
  
  exec sql FETCH FROM c1 INTO :instanceData;
  if (sqlState <> SQL_OK AND sqlState <> SQL_NOT_FOUND);
    exec sql GET DIAGNOSTICS CONDITION 1 :sqlError = MESSAGE_TEXT;
    message_escape('Could not get active service instances. ' + sqlState + ' - ' + sqlError);
  endif;
  
  dow (sqlState = SQL_OK);
    list_add(jobs : %addr(instanceData) : %size(instanceData));
    
    exec sql FETCH FROM c1 INTO :instanceData;
  enddo;
  
  return jobs;
  
  on-exit;
    exec sql CLOSE c1;
end-proc;


dcl-proc doze;
  dcl-pi *n;
    interval int(10) const;
  end-pi;

  dcl-s command char(100);
  
  command = 'DLYJOB ' + %char(interval);
  sys_executeCommand(command : %size(command));
end-proc;


dcl-proc getInterval;
  dcl-pi *n int(10) end-pi;

  dcl-s interval int(10) inz(60);
  dcl-ds setting likeds(iledash_setting_t);
  
  setting = iledash_setting_resolve(ILEDASH_SCOPE_GLOBAL : *omit : 'ILEDASH_UPDATE_INTERVAL');
  if (setting.id = 0);
    message_info('Could not get update interval from ILEDash settings. Using ' + 
        %char(interval) + ' seconds as interval.');
    return interval;
  endif;
  
  monitor;
    interval = %int(setting.value);
  on-error;
    message_info('Update interval setting is not a valid integer value. Using ' + 
        %char(interval) + ' seconds as interval.');
    return interval;
  endmon;
  
  return interval;
end-proc;


dcl-proc updateCurrentJobSetting;
  dcl-ds setting likeds(iledash_setting_t);
  
  setting = iledash_setting_resolve(ILEDASH_SCOPE_GLOBAL : *omit : 'ILEDASH_UPDATE_JOB');
  if (setting.id = 0);
    clear setting;
    setting.name = 'ILEDASH_UPDATE_JOB';
    setting.scope = ILEDASH_SCOPE_GLOBAL;
  endif;
  
  setting.value = %char(programStatus.jobNumber) + '/' +
      %trimr(programStatus.jobUser) + '/' + 
      %trimr(programStatus.jobName);
  
  iledash_setting_persist(setting);  
end-proc;


dcl-proc cleanupCurrentJobSetting;
  dcl-ds setting likeds(iledash_setting_t);
  
  setting = iledash_setting_resolve(ILEDASH_SCOPE_GLOBAL : *omit : 'ILEDASH_UPDATE_JOB');
  if (setting.id = 0);
    return;
  endif;
  
  setting.value = '';
  
  iledash_setting_persist(setting);  
end-proc;
**FREE

///
// @brief Linked List : Unit Test Sorting
//
// @author Mihael Schmidt
// @date 2018-06-07
///

//------------------------------------------------------------------------------
//                          The MIT License (MIT)
//
// Copyright (c) 2018 Mihael Schmidt
//
// Permission is hereby granted, free of charge, to any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to deal 
// in the Software without restriction, including without limitation the rights 
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
// copies of the Software, and to permit persons to whom the Software is 
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
// SOFTWARE.
//------------------------------------------------------------------------------


ctl-opt nomain;


/include 'llist_h.rpgle'
/include 'ileunit/assert_h.rpgle'

dcl-pr test_sortEmptyList end-pr;
dcl-pr test_sortSingleEntry end-pr;
dcl-pr test_sortMultipleStrings end-pr;
dcl-pr test_sortMultipleIntegers end-pr;
dcl-pr test_sortEqualStrings end-pr;
dcl-pr test_sortPartiallyEqualStringsDifferentLengths end-pr;

dcl-pr compareIntegers int(10) extproc(*dclcase);
  value1 pointer const;
  length1 int(10) const;
  entry2 pointer const;
  length2 int(10) const;
end-pr;
  

dcl-proc test_sortEmptyList export;
  dcl-s list pointer;
  
  list = list_create();
  
  list_sort(list);
  
  list_dispose(list);
end-proc;


dcl-proc test_sortSingleEntry export;
  dcl-s list pointer;
  
  list = list_create();
  list_addString(list : 'Hello World');
  list_sort(list);
  
  assert_equalsInteger(1 : list_size(list));
  assert_assertEquals('Hello World' : list_getString(list : 0));
  
  list_dispose(list);
end-proc;


dcl-proc test_sortMultipleStrings export;
  dcl-s list pointer;
  
  list = list_create();
  list_addString(list : 'This');
  list_addString(list : 'is');
  list_addString(list : 'a');
  list_addString(list : 'test');
  list_addString(list : '.');
  list_sort(list);
  
  assert_assertEquals('.' : list_getString(list : 0));
  assert_assertEquals('a' : list_getString(list : 1));
  assert_assertEquals('is' : list_getString(list : 2));
  assert_assertEquals('test' : list_getString(list : 3));
  assert_assertEquals('This' : list_getString(list : 4));
  
  list_dispose(list);
end-proc;


dcl-proc test_sortMultipleIntegers export;
  dcl-s list pointer;
  
  list = list_create();
  list_addInteger(list : 10);
  list_addInteger(list : 120);
  list_addInteger(list : 13);
  list_addInteger(list : 0);
  list_sort(list);
  
  assert_equalsInteger(0 : list_getInteger(list : 0));
  assert_equalsInteger(10 : list_getInteger(list : 1));
  assert_equalsInteger(13 : list_getInteger(list : 2));
  assert_equalsInteger(120 : list_getInteger(list : 3));
  
  list_dispose(list);
end-proc;


dcl-proc test_sortEqualStrings export;
  dcl-s list pointer;
  
  list = list_create();
  list_addString(list : 'Hello');
  list_addString(list : 'World');
  list_addString(list : 'Hello');
  list_sort(list);
  
  assert_assertEquals('Hello' : list_getString(list : 0));
  assert_assertEquals('Hello' : list_getString(list : 1));
  assert_assertEquals('World' : list_getString(list : 2));
  
  list_dispose(list);
end-proc;


dcl-proc test_sortPartiallyEqualStringsDifferentLengths export;
  dcl-s list pointer;
  
  list = list_create();
  list_addString(list : 'Hello!');
  list_addString(list : 'Hello');
  list_addString(list : 'World');
  list_sort(list);
  
  assert_assertEquals('Hello' : list_getString(list : 0));
  assert_assertEquals('Hello!' : list_getString(list : 1));
  assert_assertEquals('World' : list_getString(list : 2));
  
  list_dispose(list);
end-proc;


dcl-proc test_compareIntegers export;
  dcl-s list pointer;
  
  list = list_create();
  list_addInteger(list : 10);
  list_addInteger(list : 120);
  list_addInteger(list : 13);
  list_addInteger(list : 0);
  list_sort(list : %paddr('compareIntegers'));
  
  assert_equalsInteger(0 : list_getInteger(list : 0));
  assert_equalsInteger(10 : list_getInteger(list : 1));
  assert_equalsInteger(13 : list_getInteger(list : 2));
  assert_equalsInteger(120 : list_getInteger(list : 3));
  
  list_dispose(list);
end-proc;


dcl-proc compareIntegers export;
  dcl-pi *n int(10);
    value1 pointer const;
    length1 int(10) const;
    value2 pointer const;
    length2 int(10) const;
  end-pi;
  
  dcl-s x1 int(10) based(value1);
  dcl-s x2 int(10) based(value2);
  
  if (value1 = *null and value2 = *null);
    return 0;
  elseif (value1 = *null);
    return 1;
  elseif (value2 = *null);
    return -1;
  else;
    return x1 - x2;
  endif;  
end-proc;

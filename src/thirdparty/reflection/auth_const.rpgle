     /*********************************************************************/
     /*                                                                   */
     /* Header File Name: auth_const                                    */
     /* MI Funktion constants for Authoritie Parameter                    */
     /* translated from QSYSINC/MIH.MICOMMON                              */
     /*                                                                   */
     /*********************************************************************/
      /IF NOT DEFINED (AUTH_QRPGLEH)
      /DEFINE AUTH_QRPGLEH
     /*===================================================================*/
     D AUTH_OBJ_CTRL   C                   x'8000'
     D AUTH_OBJ_MGMT   C                   x'4000'
     D AUTH_POINTER    C                   x'8000'
     D AUTH_SPACE      C                   x'1000'
     D AUTH_RETRIEVE   C                   x'0800'
     D AUTH_INSERT     C                   x'0400'
     D AUTH_DELETE     C                   x'0200'
     D AUTH_UPDATE     C                   x'0100'
     D AUTH_OWNER      C                   x'0080'
     D AUTH_EXCLUDED   C                   x'0040'
     D AUTH_LST_MGMT   C                   x'0020'
     D AUTH_EXECUTE    C                   x'0010'
     D AUTH_ALTER      C                   x'0008'
     D AUTH_REF        C                   x'0004'
     D AUTH_ALL        C                   x'FF1C'
     D AUTH_NONE       C                   x'0000'
      /ENDIF



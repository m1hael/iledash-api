      /if not defined (TEMPLATE_AST_EXPR_NODE)
      /define TEMPLATE_AST_EXPR_NODE




      *
      *
      * NOTE : This is not implemented yet!!!
      *
      *

      



      *-------------------------------------------------------------------------
      * Template Data Structure
      *-------------------------------------------------------------------------
     D template_ast_expr_node_t...
     D                 DS                  qualified based(nullPointer) align
     D   type                         3I 0
     D   left                          *
     D   right                         *
     D   data                          *
     D   weight                       3I 0


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------
     D template_ast_expr_node_create...
     D                 PR              *   extproc('template_ast_expr_node_-
     D                                     create')
     D   serviceProgram...
     D                               10A   const
     D   userData                      *   const options(*nopass)
     D   procedureName...
     D                              256A   const options(*nopass)
      *
     D template_ast_expr_node_finalize...
     D                 PR                  extproc('template_ast_expr_node_-
     D                                     finalize')
     D   node                          *
      *
     D template_ast_expr_node_getType...
     D                 PR             3I 0 extproc('template_ast_expr_node_-
     D                                     getType')
     D   node                          *   const
      *
     D template_ast_expr_node_getStringValue...
     D                 PR         65535A   varying
     D                                     extproc('template_ast_expr_node_-
     D                                     getStringValue')
     D   node                          *   const
      *
     D template_ast_expr_node_getIntValue...
     D                 PR            20I 0 extproc('template_ast_expr_node_-
     D                                     getIntValue')
     D   node                          *   const
      *
     D template_ast_expr_node_getDecimalValue...
     D                 PR            20P 5 extproc('template_ast_expr_node_-
     D                                     getDecimalValue')
     D   node                          *   const
      *
     D template_ast_expr_node_getBooleanValue...
     D                 PR              N   extproc('template_ast_expr_node_-
     D                                     getBooleanValue')
     D   node                          *   const
      *
     D template_ast_expr_node_getLeftNode...
     D                 PR              *   extproc('template_ast_expr_node_-
     D                                     getLeftNode')
     D   node                          *   const
      *
     D template_ast_expr_node_getRightNode...
     D                 PR              *   extproc('template_ast_expr_node_-
     D                                     getRightNode')
     D   node                          *   const
      *
     D template_ast_expr_node_getWeight...
     D                 PR             3I 0 extproc('template_ast_expr_node_-
     D                                     getWeight')
     D   node                          *   const

      /endif


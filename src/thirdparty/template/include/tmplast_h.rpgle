
      /if not defined (TEMPLATE_AST)
      /define TEMPLATE_AST

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D template_ast_create...
     D                 PR              *   extproc('template_ast_create')
      *
     D template_ast_finalize...
     D                 PR                  extproc('template_ast_finalize')
     D   ast                           *
      *
     D template_ast_clear...
     D                 PR                  extproc('template_ast_clear')
     D   ast                           *   const
      *
     D template_ast_addToken...
     D                 PR                  extproc('template_ast_addToken')
     D   ast                           *   const
     D   token                         *   const
      *
     D template_ast_removeToken...
     D                 PR                  extproc('template_ast_removeToken')
     D   ast                           *   const
     D   token                         *   const
      *
     D template_ast_getTokenCount...
     D                 PR            10I 0 extproc('template_ast_getTokenCount')
     D   ast                           *   const
      *
      *
      *
     D template_ast_iterateTokens...
     D                 PR              *   extproc('template_ast_iterateTokens')
     D   ast                           *   const
      *
      *
      *
     D template_ast_parser_parse...
     D                 PR              *   extproc('template_ast_parser_parse')
     D   template                      *   const
     D   ast                           *   const options(*nopass)


      *-------------------------------------------------------------------------
      * Constants
      *-------------------------------------------------------------------------
     D TEMPLATE_AST_REFERENCE_CHAR...
     D                 C                   u'0024'                              $
      *
     D TEMPLATE_AST_DIRECTIVE_CHAR...
     D                 C                   u'0023'                              #
      *
     D TEMPLATE_AST_TOKEN_COMMENT...
     D                 C                   u'00230023'                          ##


      /endif


      /if not defined (TEMPLATE_UTIL)
      /define TEMPLATE_UTIL

     D template_ast_util_getNewUserspace...
     D                 PR              *   extproc('template_ast_util_-
     D                                     getNewUserspace')
     D   pUsLib                      10A   options(*nopass)
     D   pUsName                     10A   options(*nopass)
      *
     D template_ast_util_getRecordFormatLength...
     D                 PR            10I 0 extproc('template_ast_util_-
     D                                     getRecordFormatLength')
     D   library                     10A   const
     D   file                        10A   const
      *
     D template_ast_util_getIFSPath...
     D                 PR         65535A   varying
     D                                     extproc('template_ast_util_-
     D                                     getIFSPath')
     D  library                      10A   const
     D  file                         10A   const options(*nopass)
     D  member                       10A   const options(*nopass)


      /endif


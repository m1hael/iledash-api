      /if not defined(QUSEC)
      /define QUSEC
      /copy QSYSINC/QRPGLESRC,QUSEC
      /endif
 
      /if not defined (USERSPACE_H)
      /define USERSPACE_H
 
      *-------------------------------------------------------------------------
      * Templates
      *-------------------------------------------------------------------------
     D userspace_header_t...
     D                 DS                  qualified template
     D   userArea                    64A
     D   headerSize                  10I 0
     D   releaseLevel                 4A
     D   formatName                   8A
     D   apiUsed                     10A
     D   dateCreated                 13A
     D   infoStatus                   1A
     D   usSizeUsed                  10I 0
     D   offsetInput                 10I 0
     D   sizeInput                   10I 0
     D   offsetHeader                10I 0
     D   headerSecSiz                10I 0
     D   offsetList                  10I 0
     D   listSecSize                 10I 0
     D   nmbrEntries                 10I 0
     D   entrySize                   10I 0
     D   entryCCSID                  10I 0
     D   regionID                     2A
     D   langID                       3A
     D   subListInd                   1A
     D   us_gen_reser                42A
      *
     D userspace_attributes_t...
     D                 DS                  qualified template
     D   size                        10I 0 inz(1)
     D   key                         10I 0 inz(3)
     D   dataLength                  10I 0 inz(1)
     D   data                         1A   inz('1')
 

      *-------------------------------------------------------------------------
      * Userspace API
      *------------------------------------------------------------------------- 
      *
      * general: userspace = 10 chars userspace name + 10 chars library name
      *
     D userspace_create...
     D                 PR                  extpgm('QUSCRTUS')
     D   usName                      20A   const
     D   usExtAtt                    10A   const
     D   usSize                      10I 0 const
     D   usInitValue                  1A   const
     D   usAuthority                 10A   const
     D   usDesc                      50A   const
     D   usReplace                   10A   const
     D   usError                           likeds(QUSEC) options(*varsize)
      *
     D userspace_delete...
     D                 PR                  extpgm('QUSDLTUS')
     D   usname                      20A   const
     D   error                             likeds(QUSEC) options(*varsize)
      *
     D userspace_retrieveData...
     D                 PR                  extpgm('QUSRTVUS')
     D   usname                      20A   const
     D   startPos                    10I 0 const
     D   length                      10I 0 const
     D   receiver                 65535A   options(*varsize)
     D   error                             likeds(QUSEC)
     D                                     options(*varsize : *nopass)
      *
     D userspace_changeAttributes...
     D                 PR                  extpgm('QUSCUSAT')
     D  retLib                       10A
     D  userspace                    20A   const
     D  usAttr                       64A   options(*varsize)
     D  error                              likeds(QUSEC) options(*varsize)
      *
     D userspace_retrievePointer...
     D                 PR                  extpgm('QUSPTRUS')
     D  userspace                    20A   const
     D  pointer                        *
     D  error                       264A   const options(*nopass)
      *
     D userspace_changeData...
     D                 PR                  extpgm('QUSCHGUS')
     D   userspace                   20A   const
     D   start                       10I 0 const
     D   length                      10I 0 const
     D   data                     65535A   const options(*varsize)
     D   forceChanges                 1A   const
     D   error                             likeds(QUSEC)
     D                                     options(*varsize : *nopass)
      *
     D userspace_retrieveAttributes...
     D                 PR                  extpgm('QUSRUSAT')
     D  receiver                     30A   options(*varsize)
     D  length                       10I 0 const
     D  format                        8A   const
     D  qualUsName                   20A   const
     D  error                              likeds(QUSEC) options(*varsize)
 
      /endif

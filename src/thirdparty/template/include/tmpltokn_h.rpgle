      /if not defined (TEMPLATE_AST_TOKEN)
      /define TEMPLATE_AST_TOKEN

      *-------------------------------------------------------------------------
      * Template Data Structure
      *-------------------------------------------------------------------------
     D template_ast_token_t...
     D                 DS                  qualified based(nullPointer) align
     D   id                         100A
     D   data                          *
     D   proc_getId                    *   procptr
     D   proc_setType                  *   procptr
     D   proc_getType                  *   procptr
     D   proc_setStart...
     D                                 *   procptr
     D   proc_getStart...
     D                                 *   procptr
     D   proc_setData...
     D                                 *   procptr
     D   proc_getData...
     D                                 *   procptr
     D   proc_setLength...
     D                                 *   procptr
     D   proc_getLength...
     D                                 *   procptr
     D   proc_finalize...
     D                                 *   procptr


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------
     D template_ast_token_create...
     D                 PR              *   extproc('template_ast_token_-
     D                                     create')
     D   id                          20I 0 const
     D   serviceProgram...
     D                               10A   const
     D   userData                      *   const options(*nopass)
     D   procedureName...
     D                              256A   const options(*nopass)
      *
     D template_ast_token_finalize...
     D                 PR                  extproc('template_ast_token_-
     D                                     finalize')
     D   token                         *
      *
     D template_ast_token_getType...
     D                 PR             3I 0 extproc('template_ast_token_-
     D                                     getType')
     D   token                         *   const
      *
     D template_ast_token_setType...
     D                 PR                  extproc('template_ast_token_-
     D                                     setType')
     D   token                         *   const
     D   type                         3I 0 const
      *
     D template_ast_token_getStart...
     D                 PR            10I 0 extproc('template_ast_token_-
     D                                     getStart')
     D   token                         *   const
      *
     D template_ast_token_setStart...
     D                 PR                  extproc('template_ast_token_-
     D                                     setStart')
     D   token                         *   const
     D   start                       10I 0 const
      *
     D template_ast_token_getLength...
     D                 PR            10I 0 extproc('template_ast_token_-
     D                                     getLength')
     D   token                         *   const
      *
     D template_ast_token_setLength...
     D                 PR                  extproc('template_ast_token_-
     D                                     setLength')
     D   token                         *   const
     D   length                      10I 0 const
      *
     D template_ast_token_getData...
     D                 PR         65535A   varying extproc('template_ast_token_-
     D                                     getData')
     D   token                         *   const
      *
     D template_ast_token_setData...
     D                 PR                  extproc('template_ast_token_-
     D                                     setData')
     D   token                         *   const
     D   type                     65535A   const varying
      *
     D template_ast_token_getId...
     D                 PR            20I 0 extproc('template_ast_token_-
     D                                     getId')
     D   token                         *   const


      *-------------------------------------------------------------------------
      * Constants
      *-------------------------------------------------------------------------
     D TEMPLATE_AST_TOKEN_TYPE_UNKNOWN...
     D                 C                   0
     D TEMPLATE_AST_TOKEN_TYPE_STRING...
     D                 C                   1
     D TEMPLATE_AST_TOKEN_TYPE_COMMENT...
     D                 C                   2
     D TEMPLATE_AST_TOKEN_TYPE_REFERENCE...
     D                 C                   8
     D TEMPLATE_AST_TOKEN_TYPE_DIRECTIVE...
     D                 C                   9

      /endif


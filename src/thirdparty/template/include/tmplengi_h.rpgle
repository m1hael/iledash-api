**FREE

///
// Template Engine
//
// This is the main module where all other modules come together. Here the
// template loaders will be attached to the engine and the template context is
// passed with the template data to be merged with the template to return the
// merged template string.
//
// @author Mihael Schmidt
// @project Template Engine
///

/if not defined (TEMPLATE_ENGINE)
/define TEMPLATE_ENGINE

///
// Create template engine
//
// Returns an instance of the template engine.
//
// @return Template engine "object"
//
// @info The memory of the returned template engine must be freed after usage by
//       calling template_engine_finalize.
///
dcl-pr template_engine_create pointer extproc(*dclcase) end-pr;

///
// Dispose template engine
//
// Frees all memory allocated for the template engine.
//
// @param Template Engine
///
dcl-pr template_engine_finalize extproc(*dclcase);
  engine pointer;
end-pr;

///
// Add template loader
//
// Attaches a template loader to the template engine. Multiple template loaders
// can be attached to the same template engine instance.
//
// @param Template engine
// @param Template loader
///
dcl-pr template_engine_addTemplateLoader extproc(*dclcase);
  engine pointer const;
  loader pointer const;
end-pr;

///
// Merge data with template
//
// The data from the passed template context gets merged with the named template.
// Each attached template loader will be asked for the named template in the
// order of registration at the template engine.
//
// @param Template engine
// @param Template context containing template data
// @param Template name
//
// @return Pointer to the merged string (null-terminated)
//
// @info The caller must make sure to free the returned allocated memory
//       by calling template_memory_deallocate.
///
dcl-pr template_engine_merge pointer extproc(*dclcase);
  templateEngine pointer const;
  templateContext pointer const;
  templateName varchar(100) const;
end-pr;

/endif
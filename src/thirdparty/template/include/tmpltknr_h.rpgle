
      /if not defined (TEMPLATE_AST_TOKEN_REFERENCE)
      /define TEMPLATE_AST_TOKEN_REFERENCE

     D template_ast_token_ref_create...
     D                 PR              *   extproc('template_ast_token_-
     D                                     ref_create')
     D   id                          20I 0 const
     D   start                       10I 0 const
     D   data                          *   const
      *
     D template_ast_token_ref_finalize...
     D                 PR                  extproc('template_ast_token_-
     D                                     ref_finalize')
     D   token                         *
      *
     D template_ast_token_ref_getData...
     D                 PR         65535A   varying extproc('template_ast_token_-
     D                                     ref_getData')
     D   token                         *   const
      *
     D template_ast_token_ref_setData...
     D                 PR                  extproc('template_ast_token_-
     D                                     ref_setData')
     D   token                         *   const
     D   type                     65535A   const varying
      *
     D template_ast_token_ref_getReferenceName...
     D                 PR           100A   varying extproc('template_ast_token_-
     D                                     ref_getReferenceName')
     D   token                         *   const
      *
     D template_ast_token_ref_setQuiet...
     D                 PR                  extproc('template_ast_token_-
     D                                     ref_setQuiet')
     D   token                         *   const
     D   quiet                         N   const
      *
     D template_ast_token_ref_isQuiet...
     D                 PR              N   extproc('template_ast_token_-
     D                                     ref_isQuiet')
     D   token                         *   const

      /endif


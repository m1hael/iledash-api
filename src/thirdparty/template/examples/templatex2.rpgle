**FREE

ctl-opt dftactgrp(*no) actgrp(*caller) bnddir('TEMPLATE/TEMPLATE');


main();
*inlr = *on;


dcl-proc main;
  dcl-s command char(1000);
  dcl-s abnormallyEnded ind;
  dcl-s mergedCommand varchar(1000);
  dcl-s templateLoader pointer;
  dcl-s context pointer;
  dcl-s engine pointer;
  dcl-s mergedContentPtr pointer;
  dcl-s info char(50);
  
  command = 'CRTPF FILE(${object_library}/${object_name}) ' +
              'SRCFILE(${source_library}/${source_file}) ' + 
              'SRCMBR(${source_member})' + x'00';

  templateLoader = template_loader_memory_create(%addr(command));

  context = template_context_create();
  template_context_addCharValue(context : 'object_library':%trimr(destlibrary));
  template_context_addCharValue(context : 'object_name' : %trimr(member));
  template_context_addCharValue(context : 'source_library' : %trimr(library));
  template_context_addCharValue(context : 'source_file' : %trimr(file));
  template_context_addCharValue(context : 'source_member' : %trimr(member));
  template_context_addCharValue(context : 'source_member_type' : %trimr(type));

  engine = template_engine_create();
  template_engine_addTemplateLoader(engine : templateLoader);
  mergedContentPtr = template_engine_merge(engine : context : '*local');
  if (mergedContentPtr <> *null);
    mergedCommand = %str(mergedContentPtr);
    info = mergedCommand;
    dsply info;
  else;
    dsply 'no merged content';
  endif;

  on-exit abnormallyEnded;
    if (engine <> *null);
      template_engine_finalize(engine);
    endif;
    if (context <> *null);
      template_context_finalize(context);
    endif;
    if (templateLoader <> *null);
      template_loader_finalize(templateLoader);
    endif;
    if (mergedContentPtr <> *null);
      template_memory_deallocate(mergedContentPtr);
    endif;
  
  
end-proc;

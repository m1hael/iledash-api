**FREE

ctl-opt dftactgrp(*no) actgrp(*caller) bnddir('QC2LE' : 'MYBNDDIR' : 'TEMPLATE/TEMPLATE') debug(*yes);


//
// Prototypes
//
/include 'libc_h.rpgle'
/include 'tmplmem_h.rpgle'
/include 'tmplload_h.rpgle'
/include 'tmplcont_h.rpgle'
/include 'tmplengi_h.rpgle'


main();
*inlr = *on;


dcl-proc main;
  dcl-s templateLoader pointer;
  dcl-s templateSearchPath char(1024);
  dcl-s templateStore pointer;
  dcl-s templateEngine pointer;
  dcl-s context pointer;
  dcl-s content pointer;
  dcl-s value char(50) based(content);

  // folder where the templates are located
  // note: this is not the path to the actual template but to the parent folder!
  templateSearchPath = '/home/schmidtm/src/template/templates' + x'00';
  templateLoader = template_loader_stmf_create(%addr(templateSearchPath));

  context = template_context_create();
  template_context_addIntValue(context : 'id' : 358);
  template_context_addCharValue(context : 'email':'mihael@rpgnextgen.com');

  templateEngine = template_engine_create();
  template_engine_addTemplateLoader(templateEngine : templateLoader);
  content = template_engine_merge(templateEngine : context :
                'template04a.txt');
  template_engine_finalize(templateEngine);

  dsply value;

  template_memory_deallocate(content);
end-proc;


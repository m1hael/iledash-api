Dear customer,

your order ${id} has been received. It is composed of the following items.

Items:

## items is a map with the key/value pairs <id, item>
#foreach (${itemId} in ${items})
#set ( ${item} = ${items.get(${itemId})} )
- ${item.id} ${item.description} ${item.amount}
#end

Thanx for your order.

#if ( ${isReadyForShipping} )
It will be shipped today.
#else
It will be shipped as soon as possible.
#end

Best regards

Mihael Schmidt
Schmidt Ltd.
Germany 

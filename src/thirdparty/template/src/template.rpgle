     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      
      //
      // Prototypes
      //

      /include 'libc_h.rpgle'
      /include 'template_h.rpgle'
      /include 'tmplload_h.rpgle'
      /include 'tmplast_h.rpgle'


      //
      // Templates
      //

     D template_t      DS                  qualified template align
     D   name                       100A
     D   ast                           *
     D   rawData                       *
     D   lastChangedId...
     D                             1024A   varying
     D   templateLoaderId...
     D                                     like(templateLoaderId_t)


      //
      // Procedures
      //

     P template_create...
     P                 B                   export
     D                 PI              *
     D   name                       100A   const
      *
     D template        S               *
     D header          DS                  likeds(template_t) based(template)
      /free
       template = %alloc(%size(template_t));
       clear header;
       header.name = name;

       return template;
      /end-free
     P                 E


     P template_finalize...
     P                 B                   export
     D                 PI
     D   template                      *
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       if (template <> *null);
         if (header.ast <> *null);
           template_ast_finalize(header.ast);
         endif;

         if (header.rawdata <> *null);
           dealloc header.rawdata;
         endif;

         dealloc(n) template;
       endif;
      /end-free
     P                 E


     P template_getAst...
     P                 B                   export
     D                 PI              *
     D  template                       *   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       if (template = *null);
         // todo trigger escape message
       endif;

       return header.ast;
      /end-free
     P                 E


     P template_getName...
     P                 B                   export
     D                 PI           100A
     D   template                      *   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       if (template = *null);
         // todo trigger escape message
       endif;

       return header.name;
      /end-free
     P                 E


     P template_setAst...
     P                 B                   export
     D                 PI
     D  template                       *   const
     D  ast                            *   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       if (header.ast <> *null);
         template_ast_finalize(header.ast);
       endif;

       header.ast = ast;
      /end-free
     P                 E


     P template_getRawData...
     P                 B                   export
     D                 PI              *
     D  template                       *   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       return header.rawData;
      /end-free
     P                 E


     /**
      *
      * Copies the raw data into the template.
      *
      */
     P template_setRawData...
     P                 B                   export
     D                 PI
     D  template                       *   const
     D  rawData                        *   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       if (header.rawData <> *null);
         dealloc header.rawData;
       endif;

       header.rawData = %alloc(strlen(rawData) + 1);
       memcpy(header.rawData : rawData : strlen(rawData) + 1);
      /end-free
     P                 E


     P template_getLastChangedId...
     P                 B                   export
     D                 PI          1024A   varying
     D  template                       *   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       return header.lastChangedId;
      /end-free
     P                 E


     P template_setLastChangedId...
     P                 B                   export
     D                 PI
     D  template                       *   const
     D  lastChangedId              1024A   const varying
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       header.lastChangedId = lastChangedId;
      /end-free
     P                 E


     P template_getTemplateLoaderId...
     P                 B                   export
     D                 PI            50A
     D   template                      *   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       return header.templateLoaderId;
      /end-free
     P                 E


     P template_setTemplateLoaderId...
     P                 B                   export
     D                 PI
     D   template                      *   const
     D   templateLoaderId...
     D                               50A   const
      *
     D header          DS                  likeds(template_t) based(template)
      /free
       header.templateLoaderId = templateLoaderId;
      /end-free
     P                 E


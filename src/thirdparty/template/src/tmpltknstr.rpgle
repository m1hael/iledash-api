
     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      
      
      /include 'tmpltkna_h.rpgle'
      /include 'tmpltokn_h.rpgle'
      /include 'tmpltkns_h.rpgle'
      /include 'libc_h.rpgle'

     D TEMPLATE_AST_TOKEN_STRING_ID...
     D                 C                   'template_ast_token_string'

     D template_ast_token_string_t...
     D                 DS                  qualified based(nullPointer) align
     D   abstract                          likeds(template_ast_token_abstract_t)
     D   data                          *


     P template_ast_token_string_create...
     P                 B                   export
     D                 PI              *
     D   id                          20I 0 const
     D   start                       10I 0 const
     D   pData                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_string_t)
     D                                     based(header.data)
     D null            S              1A   inz(x'00')
      /free
       token = %alloc(%size(header));
       header.data = %alloc(%size(data));

       header.id = TEMPLATE_AST_TOKEN_STRING_ID;
       header.proc_getId = %paddr('template_ast_token_abstract_getId');
       header.proc_setType = %paddr('template_ast_token_abstract_setType');
       header.proc_getType = %paddr('template_ast_token_abstract_getType');
       header.proc_setStart = %paddr('template_ast_token_abstract_setStart');
       header.proc_getStart = %paddr('template_ast_token_abstract_getStart');
       header.proc_setLength = %paddr('template_ast_token_abstract_setLength');
       header.proc_getLength = %paddr('template_ast_token_abstract_getLength');
       header.proc_setData = %paddr('template_ast_token_string_setData');
       header.proc_getData = %paddr('template_ast_token_string_getData');
       header.proc_finalize = %paddr('template_ast_token_string_finalize');

       data.abstract.id = id;
       data.abstract.type = TEMPLATE_AST_TOKEN_TYPE_STRING;
       data.abstract.start = start;
       data.abstract.length = strlen(pData);
       data.data = %alloc(data.abstract.length + 1);
       memcpy(data.data : pData : data.abstract.length + 1); // copy the ending null too
       memcpy(data.data + data.abstract.length : %addr(null) : 1);

       return token;
      /end-free
     P                 E


     P template_ast_token_string_finalize...
     P                 B                   export
     D                 PI
     D   token                         *
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_string_t)
     D                                     based(header.data)
      /free
       if (token <> *null);

         if (header.data <> *null);
           dealloc data.data;
           dealloc header.data;
         endif;

         dealloc token;

       endif;
      /end-free
     P                 E


     P template_ast_token_string_getData...
     P                 B                   export
     D                 PI         65535A   varying
     D   token                         *   const
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_string_t)
     D                                     based(header.data)
      /free
       if (header.data <> *null);
         return %str(data.data);
       else;
         return *blank;
       endif;
      /end-free
     P                 E


     P template_ast_token_string_setData...
     P                 B                   export
     D                 PI
     D   token                         *   const
     D   pData                    65535A   const varying
      *
     D header          DS                  likeds(template_ast_token_t)
     D                                     based(token)
     D data            DS                  likeds(template_ast_token_string_t)
     D                                     based(header.data)
     D value           S          65535A   varying
     D null            S              1A   inz(x'00')
      /free
       if (header.data <> *null);
         value = pData;

         if (data.data = *null);
           data.data = %alloc(%len(value) + 1);
         else;
           data.data = %realloc(data.data : %len(value) + 1);
         endif;

         // copy data
         memcpy(data.data : %addr(value : *DATA) : %len(value));

         // add null
         memcpy(data.data + %len(value) : %addr(null) : 1);

         // update header
         data.abstract.length = %len(value);
       endif;
      /end-free
     P                 E


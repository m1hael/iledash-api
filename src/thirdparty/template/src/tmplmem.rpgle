**FREE

///
// Template Memory Management
//
// Provides procedures to allocate and deallocate memory.
//
// @author Mihael Schmidt
// @date 11.04.2018
// @project Template Engine
///


ctl-opt nomain;
/if defined(THREAD_SAFE)
ctl-opt thread(*CONCURRENT);
/endif


/include 'tmplmem_h.rpgle'


dcl-proc template_memory_allocate export;
  dcl-pi *n pointer;
    size int(10) const;
  end-pi;

  return %alloc(size);
end-proc;

dcl-proc template_memory_deallocate export;
  dcl-pi *n;
    ptr pointer;
  end-pi;

  dealloc(n) ptr;
end-proc;

dcl-proc template_memory_reallocate export;
  dcl-pi *n pointer;
    ptr pointer;
    size int(10) const;
  end-pi;

  return %realloc(ptr : size);
end-proc;

dcl-proc template_memory_discard export;
  // nothing atm
end-proc;



     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      

      /include 'libc_h.rpgle'
      /include 'lmap/lmap_h.rpgle'
      /include 'message/message_h.rpgle'
      /include 'template_h.rpgle'
      /include 'tmplload_h.rpgle'
      /include 'tmplstor_h.rpgle'
      /include 'tmplast_h.rpgle'

     D getTemplate     PR              *
     D   templateStore...
     D                                 *   const
     D   templateName               100A   const



     D template_store_t...
     D                 DS                  qualified based(nullPointer)
     D   loaders                       *
     D   templates                     *




     P template_store_create...
     P                 B                   export
     D                 PI              *
      *
     D store           S               *
     D header          DS                  likeds(template_store_t) based(store)
      /free
       store = %alloc(%size(template_store_t));

       header.loaders = lmap_create();
       header.templates = lmap_create();

       return store;
      /end-free
     P                 E


     P template_store_finalize...
     P                 B                   export
     D                 PI
     D   templateStore...
     D                                 *
      *
     D header          DS                  likeds(template_store_t)
     D                                     based(templateStore)
     D ptr             S               *
     D entry           S               *   based(ptr)
     D key             S               *
      /free
       if (templateStore <> *null);

         // clean up all registered template loaders
         if (header.loaders <> *null);
           key = lmap_iterate(header.loaders);
           dow (key <> *null);
             ptr = lmap_get(header.loaders : key : strlen(key));
             template_loader_finalize(entry);

             key = lmap_iterate(header.loaders);
           enddo;

           lmap_dispose(header.loaders);
         endif;

         // clean up all loaded/cached templates
         if (header.templates <> *null);
           key = lmap_iterate(header.templates);
           dow (key <> *null);
             ptr = lmap_get(header.templates : key : strlen(key));
             template_finalize(entry);

             key = lmap_iterate(header.templates);
           enddo;

           lmap_dispose(header.templates);
         endif;

       dealloc templateStore;

       endif;
      /end-free
     P                 E


     P template_store_loadTemplate...
     P                 B                   export
     D                 PI              *
     D   templateStore...
     D                                 *   const
     D   name                       100A   const
      *
     D templateName    S            100A
     D header          DS                  likeds(template_store_t)
     D                                     based(templateStore)
     D templateLoader  S               *
     D templateLoaderId...
     D                 S             50A
     D template        S               *
     D loadedTemplate  S               *
      /free
       templateName = name;
       template = lmap_get(header.templates :
                           %addr(templateName) : %len(templateName));

       if (template <> *null);

         // check if we must reload template
         // first we need the template loader
         templateLoaderId = template_getTemplateLoaderId(template);
         templateLoader = lmap_get(header.loaders :
                             %addr(templateLoaderId) : %size(templateLoaderId));

         if (template_loader_needTemplateReload(templateLoader : template));
           loadedTemplate = template_loader_load(templateLoader : templateName);
           template_setRawData(template : template_getRawData(loadedTemplate));
           template_setAst(template : template_ast_parser_parse(template)); // TODO error handling?!
           template_finalize(loadedTemplate);
         endif;

       else;
         template = getTemplate(templateStore : templateName);
         template_setAst(template : template_ast_parser_parse(template)); // TODO error handling?!
       endif;

       return template;
      /end-free
     P                 E


     P template_store_addTemplateLoader...
     P                 B                   export
     D                 PI
     D   templateStore...
     D                                 *   const
     D   loader...
     D                                 *   const
      *
     D templateLoader  S               *
     D header          DS                  likeds(template_store_t)
     D                                     based(templateStore)
     D templateLoaderName...
     D                 S             50A
      /free
       templateLoader = loader;

       if (templateLoader = *null);
         message_escape('Template loader mustn''t be null.');
       else;
         templateLoaderName = template_loader_getId(templateLoader);
         lmap_add(header.loaders :
                  %addr(templateLoaderName) : %size(templateLoaderName) :
                  %addr(templateLoader) : %size(templateLoader));
       endif;
      /end-free
     P                 E


     P template_store_removeTemplateLoader...
     P                 B                   export
     D                 PI
     D   templateStore...
     D                                 *   const
     D   templateLoader...
     D                                 *   const
      *
     D templateLoaderName...
     D                 S             50A
     D header          DS                  likeds(template_store_t)
     D                                     based(templateStore)
      /free
       templateLoaderName = template_loader_getId(templateLoader);
       lmap_remove(header.loaders : %addr(templateLoaderName) :
                   %size(templateLoaderName));
      /end-free
     P                 E


     P template_store_removeTemplateLoaderByName...
     P                 B                   export
     D                 PI
     D   templateStore...
     D                                 *   const
     D   templateLoaderName...
     D                               50A   const
      *
     D name            S             50A
     D header          DS                  likeds(template_store_t)
     D                                     based(templateStore)
      /free
       name = templateLoaderName;
       lmap_remove(header.loaders : %addr(name) : %size(name));
      /end-free
     P                 E


     P getTemplate     B
     D                 PI              *
     D   templateStore...
     D                                 *   const
     D   templateName               100A   const
      *
     D header          DS                  likeds(template_store_t)
     D                                     based(templateStore)
     D templateLoader  S               *   based(ptr2)
     D template        S               *
     D ptr             S               *
      /free
       ptr = lmap_iterate(header.loaders);
       dow (ptr <> *null);

         ptr2 = lmap_get(header.loaders : ptr : strlen(ptr));
         template = template_loader_load(templateLoader : templateName);
         if (template <> *null);
           lmap_abortIteration(header.loaders);
           leave;
         endif;

         ptr = lmap_iterate(header.loaders);
       enddo;

       return template;
      /end-free
     P                 E

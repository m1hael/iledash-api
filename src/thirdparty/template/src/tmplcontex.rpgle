     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      
      // TODO check key for invalid characters. valid: a-zA-Z0-9-_
      // TODO procedures for retrieving values

      /include 'tmplcont_h.rpgle'
      /include 'tmplval_h.rpgle'
      /include 'lmap/lmap_h.rpgle'
      /include 'llist/llist_h.rpgle'
      /include 'libc_h.rpgle'
      /include 'message/message_h.rpgle'

     D templateContext_t...
     D                 DS                  qualified based(nullPointer)
     D  values                         *
     D  templateStack                  *
     D  directiveStack...
     D                                 *
     D  parentContext                  *


     P template_context_create...
     P                 B                   export
     D                 PI              *
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(context)
      /free
       context = %alloc(%size(header));
       header.values = lmap_create();
       header.templateStack = list_create();
       header.directiveStack = list_create();

       return context;
      /end-free
     P                 E


     P template_context_finalize...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
      *
     D ptr             S               *
     D template        S               *   based(valuePtr)
      /free
       if (templateContext <> *null);
         lmap_abortIteration(header.values);

         // remove values from context
         ptr = lmap_iterate(header.values);
         dow (ptr <> *null);
           valuePtr = lmap_get(header.values : ptr : strlen(ptr));
           template_value_finalize(template);
           ptr = lmap_iterate(header.values);
         enddo;

         lmap_dispose(header.values);
         list_dispose(header.templateStack);
         list_dispose(header.directiveStack);

         dealloc templateContext;
       endif;
      /end-free
     P                 E


     P template_context_addIntValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                      20I 0 const
      *
     D key             S            100A   varying
     D value           S             20I 0
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_INT :
                                             %addr(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addDoubleValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                       8F   const
      *
     D key             S            100A   varying
     D value           S              8F
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_DOUBLE :
                                             %addr(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addDecimalValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                      20P 5 const
      *
     D key             S            100A   varying
     D value           S             20P 5
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_DECIMAL :
                                             %addr(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addCharValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                   65535A   const varying
      *
     D key             S            100A   varying
     D value           S          65535A   varying
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_CHAR :
                                             %addr(value:*DATA) : %len(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addListValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   value                         *   const
      *
     D key             S            100A   varying
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_LIST : value);

       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addMapValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   value                         *   const
      *
     D key             S            100A   varying
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_MAP : value);

       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addDateValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                        D   const
      *
     D key             S            100A   varying
     D value           S               D
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_DATE :
                                             %addr(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addBooleanValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                        N   const
      *
     D key             S            100A   varying
     D value           S               N
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_BOOLEAN :
                                             %addr(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addTimeValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                        T   const
      *
     D key             S            100A   varying
     D value           S               T
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_INT :
                                             %addr(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_addTimestampValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
     D   pValue                        Z   const
      *
     D key             S            100A   varying
     D value           S               Z
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D templateValue   S               *
      /free
       value = pValue;
       key = pKey;

       templateValue = template_value_create(TEMPLATE_VALUE_TYPE_INT :
                                             %addr(value));
       lmap_add(header.values : %addr(key : *DATA) : %len(key) :
                                %addr(templateValue) : %size(templateValue));
      /end-free
     P                 E


     P template_context_removeValue...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D key             S            100A   varying
      /free
       key = pKey;

       lmap_remove(header.values : %addr(key : *DATA) : %len(key));
      /end-free
     P                 E


     P template_context_getValue...
     P                 B                   export
     D                 PI         65535A   varying
     D   templateContext...
     D                                 *   const
     D   pKey                       100A   const varying
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D key             S            100A   varying
     D type            S             10I 0
     D ptr             S               *
     D templateValue   S               *   based(ptr)
      *
     D values          DS                  qualified based(valuePtr)
     D   integer                     20I 0 overlay(values)
     D   double                       8F   overlay(values)
      *
     D value           S          65535A   varying
      /free
       key = pKey;

       ptr = lmap_get(header.values : %addr(key : *DATA) : %len(key));
       if (ptr = *null);

         if (header.parentContext = *null);
           message_escape('No value in context for key ' + pKey + '.');
         else;
           return template_context_getValue(header.parentContext : key);
         endif;

       else;
         type = template_value_getType(templateValue);
         valuePtr = template_value_getValue(templateValue);

         if (type = TEMPLATE_VALUE_TYPE_INT);
           value = %char(values.integer);
         elseif (type = TEMPLATE_VALUE_TYPE_CHAR);
           value = template_value_getCharValue(templateValue);
         elseif (type = TEMPLATE_VALUE_TYPE_DOUBLE);
           value = %char(values.double);
         else;
           // TODO alle anderen typen
         endif;
       endif;

       return value;
      /end-free
     P                 E


     P template_context_pushOnTemplateStack...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pTemplate                     *   const
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D template        S               *
      /free
       template = pTemplate;

       list_add(header.values : %addr(template) : %size(template));
      /end-free
     P                 E


     P template_context_popTemplateStack...
     P                 B                   export
     D                 PI              *
     D   templateContext...
     D                                 *   const
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
      *
     D template        S               *   based(ptr)
      /free
       ptr = list_getFirst(header.values);
       list_removeFirst(header.values);
       return template;
      /end-free
     P                 E


     P template_context_pushOnDirectiveStack...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   pDirective                    *   const
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
     D directive       S               *
      /free
       directive = pDirective;

       list_add(header.values : %addr(directive) : %size(directive));
      /end-free
     P                 E


     P template_context_popDirectiveStack...
     P                 B                   export
     D                 PI              *
     D   templateContext...
     D                                 *   const
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
      *
     D directive       S               *   based(ptr)
      /free
       ptr = list_getFirst(header.values);
       list_removeFirst(header.values);
       return directive;
      /end-free
     P                 E


     P template_context_setParentContext...
     P                 B                   export
     D                 PI
     D   templateContext...
     D                                 *   const
     D   parentContext...
     D                                 *   const
      *
     D header          DS                  likeds(templateContext_t)
     D                                     based(templateContext)
      /free
       header.parentContext = parentContext;
      /end-free
     P                 E


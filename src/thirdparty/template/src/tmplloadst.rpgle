
     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      
      /include 'template_h.rpgle'
      /include 'tmplload_h.rpgle'
      /include 'libc_h.rpgle'
      /include 'reflection/reflection_h.rpgle'
      /include 'message/message_h.rpgle'
      /include 'ifsio_h.rpgle'
      /include 'rnginput/input_provider_h.rpgle'
      /include 'rnginput/streamfile2_input_provider_h.rpgle'


     D TEMPLATE_LOADER_ID...
     D                 C                   'template_loader_stmf'



     P template_loader_stmf_create...
     P                 B                   export
     D                 PI              *
     D   path                          *   const options(*nopass:*string)
      *
     D header          DS                  likeds(template_loader_t) based(ptr)
      /free
       ptr = %alloc(%size(header));

       header.id = TEMPLATE_LOADER_ID;
       header.proc_create = %paddr('template_loader_stmf_create');
       header.proc_load = %paddr('template_loader_stmf_load');
       header.proc_finalize = %paddr('template_loader_stmf_finalize');
       header.proc_needTemplateReload =
           %paddr('template_loader_stmf_needTemplateReload');

       if (%parms() = 1 or path <> *null);
         header.userdata = %alloc(strlen(path));
         memcpy(header.userdata : path : strlen(path) + 1); // +1 => also copy NULL byte
       else;
         message_escape('The template search path must be passed as userdata.');
       endif;

       return ptr;
      /end-free
     P                 E


     P template_loader_stmf_load...
     P                 B                   export
     D                 PI              *
     D   templateLoader...
     D                                 *   const
     D   templateName               100A   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D template        S               *
     D inputProvider   S               *
     D path            S           1024A   varying
     D filestats       DS                  likeds(statds) inz
     D data            S          65535A   varying
     D rawData         S               *
     D rawDataLength   S             10I 0
      /free
       path = %str(header.userdata) + '/' + %trimr(templateName) + x'00';

       // check if the file exists
       if (access(path : F_OK) < 0);
         message_escape('Template file ' + %str(%addr(path : *DATA)) +
             ' not found by template loader ' + header.id);
         return *null;
       endif;

       inputProvider = rng_input_stmf2_create(%addr(path : *DATA));
       rng_input_open(inputProvider);

       template = template_create(templateName);

       if (stat(path : filestats) = 0);
         template_setLastChangedId(template : %char(filestats.st_mtime));
       else;
         // if the real stats cannot be determined
         // then use the a number which changes constantly (like time)
         // so that the template is loaded each time again as it cannot
         // be determined if the template has been modified
         template_setLastChangedId(template : %char(%time() : *HMS));
       endif;

       dow (rng_input_read(inputProvider : data));
         // allocate enough memory for the new data
         if (rawData = *null);
           rawData = %alloc(%len(data));
         else;
           rawData = %realloc(rawData : rawDataLength + %len(data));
         endif;

         memcpy(rawData + rawDataLength : %addr(data : *DATA) : %len(data));
         rawDataLength = %len(data);
       enddo;

       // add null terminator
       rawData = %realloc(rawData : rawDataLength + 1);
       memset(rawData + rawDataLength : x'00' : 1);

       template_setRawData(template : rawData);

       rng_input_close(inputProvider);
       rng_input_finalize(inputProvider);

       return template;
      /end-free
     P                 E


     P template_loader_stmf_finalize...
     P                 B                   export
     D                 PI
     D   templateloader...
     D                                 *
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       if (header.userdata <> *null);
         dealloc header.userdata;
       endif;

       dealloc(n) templateLoader;
      /end-free
     P                 E


     P template_loader_stmf_needTemplateReload...
     P                 B                   export
     D                 PI              N
     D   templateLoader...
     D                                 *   const
     D   template                      *   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D path            S           1024A   varying
     D filestats       DS                  likeds(statds) inz
      /free
       path = %str(header.userdata) + '/' +
              %trimr(template_getName(template)) + x'00';

       // check if the file exists
       if (access(path : F_OK) < 0);
           message_escape('Template file ' + %str(%addr(path : *DATA)) +
               ' not found by template loader ' + header.id);
       endif;

       if (stat(path : filestats) = 0);
         return not ( %char(filestats.st_mtime) =
                      template_getLastChangedId(template) );
       else;
         // if the real stats cannot be determined
         // the template should be loaded again
         return *on;
       endif;
      /end-free
     P                 E


     P template_loader_stmf_getId...
     P                 B                   export
     D                 PI            50A
     D   templateLoader...
     D                                 *   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       return header.id;
      /end-free
     P                 E


     P template_loader_stmf_setId...
     P                 B                   export
     D                 PI
     D   templateLoader...
     D                                 *   const
     D   id                          50A   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       header.id = id;
      /end-free
     P                 E



     /**
      * \brief Check template loader
      *
      * Checks if the template loader provides procedure pointers for all
      * procedures it should implement.
      *
      * \param Template Loader
      *
      * \throws CPF9898 Incomplete implementation
      */
     P template_loader_check...
     P                 B
     D                 PI
     D   templateLoader...
     D                                 *   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D cachedTemplateLoaderId...
     D                 S                   like(template_loader_t.id)
     D                                     static
     D templateLoaderChecked...
     D                 S               N   inz(*off) static
      /free
       if (header.id = cachedTemplateLoaderId and
           templateLoaderChecked);
         return;
       endif;

       cachedTemplateLoaderId = header.id;

       if (header.proc_create = *null);
         message_escape('Template Loader Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.');
       elseif (header.proc_finalize = *null);
         message_escape('Template Loader Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.');
       elseif (header.proc_load = *null);
         message_escape('Template Loader Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.');
       endif;

       templateLoaderChecked = *on;
      /end-free
     P                 E



     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      
      /include 'template_h.rpgle'
      /include 'tmplload_h.rpgle'
      /include 'libc_h.rpgle'
      /include 'reflection/reflection_h.rpgle'
      /include 'message/message_h.rpgle'


     D TEMPLATE_LOADER_ID...
     D                 C                   'template_loader_stmf'
     D TEMPLATE_LOADER_MEMORY_ID...
     D                 C                   'template_loader_memory'





     P template_loader_create...
     P                 B                   export
     D                 PI              *
     D   serviceProgram...
     D                               10A   const
     D   userData                      *   const options(*nopass)
     D   procedureName...
     D                              256A   const options(*nopass)
      *
     D createProcedureName...
     D                 S            256A
     D procedurePointer...
     D                 S               *   procptr
     D create          PR              *   extproc(procedurePointer)
     D    userdata                     *   const options(*nopass)
      *
     D templateLoaderHeader...
     D                 DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       if (%parms() = 3);
         createProcedureName = procedureName;
       else;
         createProcedureName = 'template_loader_create';
       endif;

       procedurePointer = reflection_getProcedurePointer(
                                  serviceProgram :
                                  %trimr(createProcedureName));

       if (procedurePointer <> *null);

           // input provider create/build
           if (%parms() = 1);
             templateLoader = create();
           else;
             templateLoader = create(userData);
           endif;

       endif;

       return templateLoader;
      /end-free
     P                 E


     P template_loader_load...
     P                 B                   export
     D                 PI              *
     D   templateLoader...
     D                                 *   const
     D   templateName               100A   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D procedurePointer...
     D                 S               *   procptr
     D load            PR              *   extproc(procedurePointer)
     D   templateLoader...
     D                                 *   const
     D   templateName               100A   const

      /free
       procedurePointer = header.proc_load;

       template_loader_check(templateLoader);

       return load(templateLoader : templateName);
      /end-free
     P                 E


     P template_loader_needTemplateReload...
     P                 B                   export
     D                 PI              N
     D   templateLoader...
     D                                 *   const
     D   template                      *   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D procedurePointer...
     D                 S               *   procptr
     D needTemplateReload...
     D                 PR              N   extproc(procedurePointer)
     D   templateLoader...
     D                                 *   const
     D   template                      *   const
      /free
       procedurePointer = header.proc_needTemplateReload;

       template_loader_check(templateLoader);

       return needTemplateReload(templateLoader : template);
      /end-free
     P                 E


     /**
      * \brief Dispose template loader
      *
      * Calls clean up process and frees allocated memory.
      *
      * \param Template loader
      */
     P template_loader_finalize...
     P                 B                   export
     D                 PI
     D   templateLoader...
     D                                 *
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D procedurePointer...
     D                 S               *   procptr
     D finalize        PR                  extproc(procedurePointer)
     D   templateLoader...
     D                                 *
      /free
       procedurePointer = header.proc_finalize;

       template_loader_check(templateLoader);

       finalize(templateLoader);

       if (templateLoader <> *null);
         dealloc(n) templateLoader;
       endif;
      /end-free
     P                 E


     P template_loader_getId...
     P                 B                   export
     D                 PI            50A
     D   templateLoader...
     D                                 *   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       return header.id;
      /end-free
     P                 E


     P template_loader_setId...
     P                 B                   export
     D                 PI
     D   templateLoader...
     D                                 *   const
     D   id                          50A   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       header.id = id;
      /end-free
     P                 E


     /**
      * \brief Check template loader
      *
      * Checks if the template loader provides procedure pointers for all
      * procedures it should implement.
      *
      * \param Template Loader
      *
      * \throws CPF9898 Incomplete implementation
      */
     P template_loader_check...
     P                 B
     D                 PI
     D   templateLoader...
     D                                 *   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D cachedTemplateLoaderId...
     D                 S                   like(template_loader_t.id)
     D                                     static
     D templateLoaderChecked...
     D                 S               N   inz(*off) static
      /free
       if (header.id = cachedTemplateLoaderId and
           templateLoaderChecked);
         return;
       endif;

       cachedTemplateLoaderId = header.id;

       if (header.proc_create = *null);
         message_escape('Template Loader Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.');
       elseif (header.proc_finalize = *null);
         message_escape('Template Loader Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.');
       elseif (header.proc_load = *null);
         message_escape('Template Loader Interface: ' +
             'Incomplete Implementation ' + %trimr(header.id) +  '.');
       endif;

       templateLoaderChecked = *on;
      /end-free
     P                 E



     /**
      *
      *
      * The template source code must be passed as a parameter.
      *
      */
     P template_loader_memory_create...
     P                 B                   export
     D                 PI              *
     D   path                          *   const options(*nopass:*string)
      *
     D header          DS                  likeds(template_loader_t) based(ptr)
      /free
       ptr = %alloc(%size(header));

       header.id = TEMPLATE_LOADER_MEMORY_ID;
       header.proc_create = %paddr('template_loader_memory_create');
       header.proc_load = %paddr('template_loader_memory_load');
       header.proc_finalize = %paddr('template_loader_memory_finalize');
       header.proc_needTemplateReload =
           %paddr('template_loader_memory_needTemplateReload');

       if (%parms() = 1 or path <> *null);
         header.userdata = %alloc(strlen(path) + 1);
         memcpy(header.userdata : path : strlen(path) + 1); // +1 => also copy NULL byte
       else;
         message_escape('The template source code must be passed as userdata.');
       endif;

       return ptr;
      /end-free
     P                 E


     /**
      * \brief Load template
      *
      * The passed template name is irrelevant in this implementation
      * (but will be saved) as the template source code has already been
      * passed to the loader on creation.
      *
      */
     P template_loader_memory_load...
     P                 B                   export
     D                 PI              *
     D   templateLoader...
     D                                 *   const
     D   templateName               100A   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
     D template        S               *
      /free
       template = template_create(templateName);

       template_setRawData(template : header.userdata);

       return template;
      /end-free
     P                 E


     P template_loader_memory_finalize...
     P                 B                   export
     D                 PI
     D   templateloader...
     D                                 *
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       if (header.userdata <> *null);
         dealloc header.userdata;
       endif;

       dealloc(n) templateLoader;
      /end-free
     P                 E


     P template_loader_memory_needTemplateReload...
     P                 B                   export
     D                 PI              N
     D   templateLoader...
     D                                 *   const
     D   template                      *   const
      /free
       return *off; // the template source will never change for this implementation
      /end-free
     P                 E

     P template_loader_memory_getId...
     P                 B                   export
     D                 PI            50A
     D   templateLoader...
     D                                 *   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       return header.id;
      /end-free
     P                 E


     P template_loader_memory_setId...
     P                 B                   export
     D                 PI
     D   templateLoader...
     D                                 *   const
     D   id                          50A   const
      *
     D header          DS                  likeds(template_loader_t)
     D                                     based(templateLoader)
      /free
       header.id = id;
      /end-free
     P                 E


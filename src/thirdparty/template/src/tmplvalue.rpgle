
     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      
      
      /include 'tmplval_h.rpgle'
      /include 'tmplmem_h.rpgle'
      /include 'llist/llist_h.rpgle'
      /include 'lmap/lmap_h.rpgle'
      /include 'libc_h.rpgle'
      /include 'message/message_h.rpgle'


     D template_value_t...
     D                 DS                  qualified template
     D   type                        10I 0
     D   value                         *
     D   length                      10I 0
      *
     D types           DS                  qualified template
     D   integer                     10I 0
     D   long                        20I 0
     D   boolean                       N
     D   double                       8F
     D   date                          D
     D   time                          T
     D   timestamp                     Z
     D   list                          *
     D   map                           *
     D   decimal                     20P 5
     D   pointer                       *


     P template_value_create...
     P                 B                   export
     D                 PI              *
     D   type                        10I 0 const
     D   value                         *   const
     D   length                      10I 0 const options(*nopass)
      *
     D header          DS                  likeds(template_value_t)
     D                                     based(templateValue)
     D tmp             S               *
      /free
       templateValue = template_memory_allocate(%size(template_value_t));

       clear header;
       header.type = type;

       select;
         when (type = TEMPLATE_VALUE_TYPE_BOOLEAN);
           header.value = template_memory_allocate(%size(types.boolean));
           memcpy(header.value : value : %size(types.boolean));

         when (type = TEMPLATE_VALUE_TYPE_CHAR);
           header.value = template_memory_allocate(length);
           header.length = length;
           memcpy(header.value : value : length);

         when (type = TEMPLATE_VALUE_TYPE_DATE);
           header.value = template_memory_allocate(%size(types.date));
           memcpy(header.value : value : %size(types.date));

         when (type = TEMPLATE_VALUE_TYPE_DOUBLE);
           header.value = template_memory_allocate(%size(types.double));
           memcpy(header.value : value : %size(types.double));

         when (type = TEMPLATE_VALUE_TYPE_INT);
           header.value = template_memory_allocate(%size(types.long));
           memcpy(header.value : value : %size(types.long));

         when (type = TEMPLATE_VALUE_TYPE_LIST);
           header.value = template_memory_allocate(%size(types.list));
           tmp = list_copy(value);
           memcpy(header.value : %addr(tmp) : %size(types.list));

         when (type = TEMPLATE_VALUE_TYPE_MAP);
           header.value = template_memory_allocate(%size(types.map));
           memcpy(header.value : value : %size(types.map));
           // TODO lmap needs copy procedure

         when (type = TEMPLATE_VALUE_TYPE_TIME);
           header.value = template_memory_allocate(%size(types.time));
           memcpy(header.value : value : %size(types.time));

         when (type = TEMPLATE_VALUE_TYPE_TIMESTAMP);
           header.value = template_memory_allocate(%size(types.timestamp));
           memcpy(header.value : value : %size(types.timestamp));

         when (type = TEMPLATE_VALUE_TYPE_DECIMAL);
           header.value = template_memory_allocate(%size(types.decimal));
           memcpy(header.value : value : %size(types.decimal));


         other;
           message_escape('[TemplateValue] Unsupported value type ' +
                          %char(type) + ' for template value creation.');
       endsl;

       return templateValue;
      /end-free
     P                 E


     P template_value_finalize...
     P                 B                   export
     D                 PI
     D   templateValue...
     D                                 *
      *
     D header          DS                  likeds(template_value_t)
     D                                     based(templateValue)
     D tmp             S               *   based(header.value)
      /free
       if (templateValue <> *null);

         if (header.value <> *null);

           if (header.type = TEMPLATE_VALUE_TYPE_LIST);
             list_dispose(tmp);
           elseif (header.type = TEMPLATE_VALUE_TYPE_MAP);
             lmap_dispose(tmp);
           else;
             dealloc header.value;
           endif;
         endif;

         dealloc(n) templateValue;

       endif;
      /end-free
     P                 E


     P template_value_getType...
     P                 B                   export
     D                 PI            10I 0
     D   templateValue...
     D                                 *   const
      *
     D header          DS                  likeds(template_value_t)
     D                                     based(templateValue)
      /free
       if (templateValue = *null);
         message_escape('[TemplateValue] Passed pointer is no template value.');
       else;
         return header.type;
       endif;
      /end-free
     P                 E


     P template_value_getValue...
     P                 B                   export
     D                 PI              *
     D   templateValue...
     D                                 *   const
      *
     D header          DS                  likeds(template_value_t)
     D                                     based(templateValue)
      /free
       if (templateValue = *null);
         message_escape('[TemplateValue] Passed pointer is no template value.');
       else;
         return header.value;
       endif;
      /end-free
     P                 E

     P template_value_getCharValue...
     P                 B                   export
     D                 PI         65535A   varying
     D   templateValue...
     D                                 *   const
      *
     D value           S          65535A   varying
      *
     D header          DS                  likeds(template_value_t)
     D                                     based(templateValue)
      /free
       if (templateValue = *null);
         message_escape('[TemplateValue] Passed pointer is no template value.');
       else;
         value = %str(header.value : header.length);
         return value;
       endif;
      /end-free
     P                 E

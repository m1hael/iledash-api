     /**
      * \brief Template Engine : Main Module
      *
      * \author Mihael Schmidt
      * \date   16.01.2011
      *
      */


     H nomain
      /if defined(THREAD_SAFE)
     H thread(*CONCURRENT)
      /endif
      

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
      /include 'llist/llist_h.rpgle'
      /include 'lmap/lmap_h.rpgle'
      /include 'unicode_c.rpgle'
      /include 'message/message_h.rpgle'
      /include 'libc_h.rpgle'
      /include 'tmplengi_h.rpgle'
      /include 'tmplmem_h.rpgle'
      /include 'tmplstor_h.rpgle'
      /include 'tmplload_h.rpgle'
      /include 'template_h.rpgle'
      /include 'tmplast_h.rpgle'
      /include 'tmpltokn_h.rpgle'
      /include 'tmpltkns_h.rpgle'
      /include 'tmpltknc_h.rpgle'
      /include 'tmpltknr_h.rpgle'
      /include 'tmplcont_h.rpgle'


      *-------------------------------------------------------------------------
      * Template Data Structure
      *-------------------------------------------------------------------------
     D template_engine_t...
     D                 DS                  qualified based(nullPointer) align
     D   store                         *


      *-------------------------------------------------------------------------
      * Constants
      *-------------------------------------------------------------------------
     D INITIAL_MERGE_SIZE...
     D                 C                   4096


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------

     P template_engine_create...
     P                 B                   export
     D                 PI              *
      *
     D header          DS                  likeds(template_engine_t)
     D                                     based(templateEngine)
      /free
       templateEngine = template_memory_allocate(%size(header));

       header.store = template_store_create();

       return templateEngine;
      /end-free
     P                 E


     P template_engine_finalize...
     P                 B                   export
     D                 PI
     D   templateEngine...
     D                                 *
      *
     D header          DS                  likeds(template_engine_t)
     D                                     based(templateEngine)
      /free
       template_store_finalize(header.store);
       dealloc templateEngine;
      /end-free
     P                 E


     P template_engine_addTemplateLoader...
     P                 B                   export
     D                 PI
     D   engine                        *   const
     D   loader                        *   const
      *
     D header          DS                  likeds(template_engine_t)
     D                                     based(engine)
      /free
       template_store_addTemplateLoader(header.store : loader);
      /end-free
     P                 E


     P template_engine_merge...
     P                 B                   export
     D                 PI              *
     D   templateEngine...
     D                                 *   const
     D   templateContext...
     D                                 *   const
     D   templateName               100A   const varying
      *
     D header          DS                  likeds(template_engine_t)
     D                                     based(templateEngine)
      *
     D ast             S               *
     D token           S               *   based(tokenPtr)
     D tokenType       S             10I 0
     D tokenLength     S             10I 0
     D template        S               *
     D refId           S            100A   varying
     D runtimeContext  S               *
     D mergedContent   S               *
     D mergedContentCurrentPosition...
     D                 S               *
     D mergedContentSize...
     D                 S             10I 0
     D value           S          65535A   varying
     D allocatedSize   S             10I 0
     D NULL            S              1A   inz(x'00')
     
      /free
       // TODO check for reallocation
       // TODO reallocate memory with same strategy as Java Vector (x2 of current size)

       runtimeContext = template_context_create();
       template_context_setParentContext(runtimeContext : templateContext);

       template = template_store_loadTemplate(header.store : templateName);
       ast = template_getAst(template);

       monitor;
         mergedContent = template_memory_allocate(INITIAL_MERGE_SIZE);
         allocatedSize = INITIAL_MERGE_SIZE;
         mergedContentCurrentPosition = mergedContent;
         mergedContentSize = INITIAL_MERGE_SIZE;

         tokenPtr = template_ast_iterateTokens(ast);
         dow (tokenPtr <> *null);
           value = template_ast_token_getData(token);
           tokenType = template_ast_token_getType(token);

           if (tokenType = TEMPLATE_AST_TOKEN_TYPE_STRING);
             memcpy(mergedContentCurrentPosition : %addr(value : *data) :
                 %len(value));
             mergedContentCurrentPosition += %len(value);
           elseif (tokenType = TEMPLATE_AST_TOKEN_TYPE_REFERENCE);
             refId = template_ast_token_ref_getReferenceName(token);

             monitor;
               value = template_context_getValue(runtimeContext : refId);
               memcpy(mergedContentCurrentPosition : %addr(value : *data) :
                   %len(value));
               mergedContentCurrentPosition += %len(value);
               on-error;
                 message_info('Skipping reference ' + refId +
                     '. No value mapped.');
             endmon;

           elseif (tokenType = TEMPLATE_AST_TOKEN_TYPE_DIRECTIVE);
             // TODO handle directives here
           endif;

           tokenPtr = template_ast_iterateTokens(ast);
         enddo;

         memcpy(mergedContentCurrentPosition : %addr(NULL) : 1);
           
         on-error;
           // TODO error handling
           dsply 'error';
       endmon;
       
       template_context_finalize(runtimeContext);

       return mergedContent;
      /end-free
     P                 E


#
# ILEastic Dashboard
#
#
# To start the web services manually: 
#
# SBMJOB JOBD(ILEDASH/ILEDASH) USER(*CURRENT) RQSDTA(*JOBD) INLLIBL(*JOBD)
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the program objects.
BIN_LIB=ILEDASH

WEB_USER=ILEDASH

#
# User-defined part end
#-------------------------------------------------------------------------------


all: env compile bind

env: messages journal
	-system "CRTSBSD SBSD($(BIN_LIB)/ILEDASH) POOLS((1 *BASE)) TEXT('ILEDash : ILEastic Dashboard')"
	-system "CRTCLS CLS($(BIN_LIB)/ILEDASH) TEXT('ILEDash : ILEastic Dashboard')"
	-system "CRTJOBQ JOBQ($(BIN_LIB)/ILEDASH) TEXT('ILEDash : ILEastic Dashboard')"
	-system "CRTJOBQ JOBQ($(BIN_LIB)/ILEDASHUPD) TEXT('ILEDash : Status Updater')"
	-system "ADDJOBQE SBSD($(BIN_LIB)/ILEDASH) JOBQ($(BIN_LIB)/ILEDASH) MAXACT(*NOMAX) SEQNBR(10)"
	-system "ADDJOBQE SBSD($(BIN_LIB)/ILEDASH) JOBQ($(BIN_LIB)/ILEDASHUPD) MAXACT(1) SEQNBR(20)"
	-system "ADDRTGE SBSD($(BIN_LIB)/ILEDASH) SEQNBR(9999) CMPVAL(*ANY) PGM(QCMD) CLS($(BIN_LIB)/ILEDASH) MAXACT(100)"
	-system "CRTJOBD JOBD($(BIN_LIB)/ILEDASH) JOBQ(BGOBJ/BGWS) USER($(WEB_USER)) RQSDTA('CALL PGM(ILEDASH)') INLLIBL(ILEDASH SHARED ILEASTIC) ALWMLTTHD(*YES) TEXT('ILEDash : Web Services')"
	-system "CRTJOBD JOBD($(BIN_LIB)/ILEDASHSTR) JOBQ(BGOBJ/BGWS) USER($(WEB_USER)) RQSDTA('CALL PGM(ILEDASHSTR)') INLLIBL($(BIN_LIB) SHARED ILEASTIC) ALWMLTTHD(*YES) TEXT('ILEDash : Web Services Starter')"
	-system "CRTJOBD JOBD($(BIN_LIB)/ILEDASHUPD) JOBQ($(BIN_LIB)/ILEDASHUPD) USER($(WEB_USER)) RQSDTA('CALL PGM(ILEDUPDATE)') INLLIBL($(BIN_LIB) SHARED) TEXT('ILEDash : Status Updater')"
	-system "ADDAJE SBSD($(BIN_LIB)/ILEDASH) JOB(ILEDASH) JOBD($(BIN_LIB)/ILEDASH)"
	-system "ADDAJE SBSD($(BIN_LIB)/ILEDASH) JOB(ILEDASHSTR) JOBD($(BIN_LIB)/ILEDASHSTR)"
	-system "ADDAJE SBSD($(BIN_LIB)/ILEDASH) JOB(ILEDASHUPD) JOBD($(BIN_LIB)/ILEDASHUPD)"

journal:
	-system "CRTJRNRCV JRNRCV($(BIN_LIB)/ILEDASH) TEXT('ILEDash : Journal Receiver')"
	-system "CRTJRN JRN($(BIN_LIB)/ILEDASH) JRNRCV($(BIN_LIB)/ILEDASH) MNGRCV(*SYSTEM) DLTRCV(*YES) TEXT('ILEDash : Journal')"

messages:
	-system "CRTMSGF MSGF($(BIN_LIB)/ILEDASH) TEXT('ILEDash Messages')"
	-system -i "ADDMSGD DAS0001 MSGF($(BIN_LIB)/ILEDASH) MSG('Version mismatch') SECLVL(*NONE)"
	-system -i "ADDMSGD DAS0002 MSGF($(BIN_LIB)/ILEDASH) MSG('Could not save the data because of referential integrity.') SECLVL(*NONE)"
	-system -i "ADDMSGD DAS0003 MSGF($(BIN_LIB)/ILEDASH) MSG('Could not save the data because of not matching a database constraint.') SECLVL(*NONE)"
	-system -i "ADDMSGD DAS0004 MSGF($(BIN_LIB)/ILEDASH) MSG('Could not save the data because of duplicate key.') SECLVL(*NONE)"
	-system -i "ADDMSGD DAS0005 MSGF($(BIN_LIB)/ILEDASH) MSG('Could not get a free configuration for the service.') SECLVL(*NONE)"
	-system -i "ADDMSGD DAS0006 MSGF($(BIN_LIB)/ILEDASH) MSG('Could not resolve all placeholders for command.') SECLVL(*NONE)"


compile:
	$(MAKE) -C src/ compile $*
	-system "STRJRNPF FILE($(BIN_LIB)/*ALL) JRN($(BIN_LIB)/ILEDASH) IMAGES(*BOTH)"

bind:
	$(MAKE) -C src/ bind $*
	
clean:
	$(MAKE) -C src/ clean $*

purge:
	-system -i "ENDJRNPF FILE(*ALL) JRN($(BIN_LIB)/ILEDASH)"
	-system -i "DLTJRN JRN($(BIN_LIB)/ILEDASH)"
	-system -i "DLTMSGF MSGF($(BIN_LIB)/ILEDASH)"
	$(MAKE) -C src/ purge $*

.PHONY:
